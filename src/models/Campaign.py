from marshmallow import Schema, fields
from src.extensions import db, ma
from flask_restful_swagger import swagger

@swagger.model
class Campaign(db.Model):
    __tablename__ = 'campaigns'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), unique=True, nullable=False)

    def __init__(self, name):
        self.name = name

class CampaignSchema(ma.Schema):
    id = fields.Integer()
    name = fields.String(required=True)
