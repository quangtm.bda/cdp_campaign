from flask_restful import Resource
from src.models import Campaign, CampaignSchema
from flask_restful_swagger import swagger

campaigns_schema = CampaignSchema(many=True)
campaign_schema = CampaignSchema()


class Campaigns(Resource):
    "Describing campaigns"
    @swagger.operation(
        notes='some really good notes',
        responseClass=Campaign.__name__,
        parameters=[
            {
              "name": "type",
              "description": "get campaigns by type",
              "required": True,
              "allowMultiple": False,
              "dataType": 'string',
              "paramType": "path"
            }
          ],
        responseMessages=[
            {
              "code": 201,
              "message": "Created. The URL of the created blueprint should be in the Location header"
            },
            {
              "code": 405,
              "message": "Invalid input"
            }
          ]
        )
    def get(self):
        campaigns = Campaign.query.all()
        campaigns = campaigns_schema.dump(campaigns).data
        return {"message": "Hello, World!", "data": campaigns}, 200
