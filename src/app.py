from flask import Flask
from flask_restful import Api
from flask_restful_swagger import swagger
from flask_cors import CORS

from src.extensions import db
from src.resources.Campaigns import Campaigns

def create_app(config_filename):
    app = Flask(__name__)
    app.config.from_object(config_filename)
    
    ###################################
    # Wrap the Api with swagger.docs. It is a thin wrapper around the Api class that adds some swagger smarts
    api = swagger.docs(Api(app, catch_all_404s=True),
            apiVersion='1',
            description='Campaign service API',
            api_spec_url='/v1/api-docs')
    ###################################

    cors = CORS(app, resources={r"*": {"origins": "*"}})
    db.init_app(app)
    add_resource(api)

    return app

def add_resource(api):
    api.add_resource(Campaigns, '/v1/campaigns')
