FROM python:3.6-alpine

WORKDIR /restapi

COPY ./ .

RUN apk add --no-cache tzdata && cp /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime \
    && echo "Asia/Ho_Chi_Minh" > /etc/timezone

RUN apk upgrade -U \
    && apk add --no-cache -u ca-certificates libva-intel-driver supervisor postgresql-dev gcc python3-dev musl-dev build-base linux-headers pcre-dev \
    && rm -rf /tmp/* /var/cache/*

RUN pip --no-cache-dir install -r requirements.txt && mkdir -p /var/log/apps

EXPOSE 3000

ENTRYPOINT python server.py run

