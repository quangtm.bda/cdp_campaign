from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from src.extensions import db
from src.app import create_app

app = create_app("src.config")

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

@manager.command
def run(port=3000):
    print('api running...')
    app.run(debug=True, port=3000, host='0.0.0.0')

if __name__ == "__main__":
    manager.run()

